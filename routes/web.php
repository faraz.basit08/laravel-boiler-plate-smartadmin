<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'login']);
Route::get('login', [UserController::class, 'login'])->name('login');
Route::post('login_submit', [UserController::class, 'loginSubmit'])->name('login_submit');
Route::get('logout', [UserController::class, 'logout'])->name('logout');
Route::get('home', [HomeController::class, 'index'])->name('home_page');
Route::get('user_index', [UserController::class, 'index'])->name('user_index');
Route::get('user_create', [UserController::class, 'create'])->name('user_create');
Route::post('user_post', [UserController::class, 'store'])->name('user_post');
Route::get('user_edit/{id}', [UserController::class, 'edit'])->name('user_edit');
Route::post('user_update', [UserController::class, 'update'])->name('user_update'); 
Route::post('user_destroy', [UserController::class, 'destroy'])->name('user_destroy'); 