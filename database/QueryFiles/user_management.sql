/*
SQLyog Trial v13.1.8 (64 bit)
MySQL - 10.4.25-MariaDB : Database - laravel_user_management
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`laravel_user_management` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `laravel_user_management`;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company_id` bigint(20) DEFAULT NULL,
  `login_id` varchar(255) DEFAULT NULL,
  `emp_code` varchar(255) DEFAULT NULL,
  `emp_id` bigint(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `profile_img` varchar(1024) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `designation_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) DEFAULT NULL,
  `pb_code` varchar(255) DEFAULT NULL,
  `land_line` int(11) DEFAULT NULL,
  `ip` int(11) DEFAULT NULL,
  `date_of_joining` datetime DEFAULT NULL,
  `date_of_resign` datetime DEFAULT NULL,
  `date_of_transfer` datetime DEFAULT NULL,
  `is_resigned` enum('yes','no') DEFAULT 'no',
  `creation_date` datetime DEFAULT NULL,
  `android_push_id` varchar(1024) DEFAULT NULL,
  `apple_push_id` varchar(1024) DEFAULT NULL,
  `num_of_login_attempts` int(10) unsigned DEFAULT NULL,
  `last_login_time` int(10) unsigned DEFAULT NULL,
  `is_new_user` enum('yes','no') DEFAULT 'yes',
  `last_change_password_date` datetime DEFAULT NULL,
  `token` varchar(1024) DEFAULT NULL,
  `is_live` int(11) DEFAULT 0,
  `temp_key` tinyint(4) DEFAULT 0,
  `device_id` varchar(255) DEFAULT NULL,
  `parallel_to` varchar(255) DEFAULT NULL,
  `email_verification_at` timestamp NULL DEFAULT NULL,
  `email_verification_status` enum('not verified','verified') DEFAULT 'not verified',
  `activation_code` varchar(1024) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 2,
  `is_enable` tinyint(4) DEFAULT 1,
  `comments` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `login_id` (`login_id`),
  KEY `user_type` (`user_type`),
  KEY `first_name` (`first_name`),
  KEY `creation_date` (`creation_date`),
  KEY `user_type_2` (`user_type`),
  KEY `first_name_2` (`first_name`),
  KEY `creation_date_2` (`creation_date`),
  KEY `branch` (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`company_id`,`login_id`,`emp_code`,`emp_id`,`password`,`first_name`,`last_name`,`email_address`,`phone_number`,`gender`,`profile_img`,`date_of_birth`,`user_type`,`designation_id`,`branch_id`,`pb_code`,`land_line`,`ip`,`date_of_joining`,`date_of_resign`,`date_of_transfer`,`is_resigned`,`creation_date`,`android_push_id`,`apple_push_id`,`num_of_login_attempts`,`last_login_time`,`is_new_user`,`last_change_password_date`,`token`,`is_live`,`temp_key`,`device_id`,`parallel_to`,`email_verification_at`,`email_verification_status`,`activation_code`,`status`,`is_enable`,`comments`,`created_at`,`created_by`,`updated_at`,`updated_by`) values 
(1,NULL,NULL,NULL,NULL,'$2y$10$xW2BgDpJrPZoGvfG74lIj.dIVgYvEffETAE.ZK5.kuJOdM4xjSAzu','Moses','Savage','hajum@mailinator.com','+1 (976) 837-8035',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-23 11:34:07',NULL,'2022-11-24 15:22:21',NULL),
(2,NULL,NULL,NULL,NULL,'$2y$10$voqXckRJuKY1RYed.t/FjuxpZVqKmOuJNM7bD25FtL6bbwJWjkjcu','Orson','Vasquez','lasulot@mailinator.com','+1 (201) 427-7919',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-23 11:46:49',NULL,'2022-11-24 15:21:03',NULL),
(3,NULL,NULL,NULL,NULL,'$2y$10$8rC1XiIIDjBIPBYV0lYzLOFVQFhP6hntN93MfKBNOUOSvn1Ch7lgq','Dominic','Grimes','zaqut@mailinator.com','+1 (445) 227-3787',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-23 11:50:23',NULL,'2022-11-24 15:17:30',NULL),
(4,NULL,NULL,NULL,NULL,'$2y$10$bOnebTtoYBO/ShY8n3etvuBE/nERCpI1n7wd1BFfCPDNDHWxOXNO2','Emerson','Lane','qimuke@mailinator.com','+1 (117) 695-5545',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-23 11:54:30',NULL,'2022-11-24 15:17:18',NULL),
(5,NULL,NULL,NULL,NULL,'$2y$10$Zf4cT9YbSfYMs7krCgYITuvJ2m18JfxlCfSpIILgvh7GoKnLJ/7iy','Germane','Sears','foxoraveny@mailinator.com','+1 (908) 814-2175',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,2,NULL,'2022-11-23 11:57:23',NULL,'2022-11-24 15:23:50',NULL),
(6,NULL,NULL,NULL,NULL,'$2y$10$o8fsTKSc.vTPTI9I29vyUOaxs9cv7ZV80X47JD95C8qgOUi135hzG','Upton','Meadows','duqahokig@mailinator.com','+1 (819) 138-2804',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,2,NULL,'2022-11-23 12:00:47',NULL,'2022-11-24 15:23:18',NULL),
(285,NULL,NULL,NULL,NULL,'$2y$10$GhuRAoQbDIzNWKLOWBwsvus99dtezd62XfeQA5uYb7zct6Kqm9gQi','usama1','khan1','usama@gtmail.com1','usama1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,2,NULL,'2022-11-24 15:26:02',NULL,'2022-11-24 15:27:35',NULL),
(286,NULL,NULL,NULL,NULL,'$2y$10$W6XHGXp/uh/OLf3LH1uFhey0k/4/HF38JZuQvTXdehdD1i5dGaGwG','Bernard','Estes','vucyhip@mailinator.com','+1 (201) 295-8563',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,2,NULL,'2022-11-24 15:34:17',NULL,'2022-11-24 17:39:27',NULL),
(287,NULL,NULL,NULL,NULL,'$2y$10$FrnDuc5SIdqzF2xHD3jNWefAYquy.BOELug6GkLFl548Qp2shrahS','Xantha','Allen','name@mailinator.com','+1 (701) 764-3198',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-24 16:12:29',NULL,'2022-11-24 16:12:29',NULL),
(288,NULL,NULL,NULL,NULL,'$2y$10$Uov6GWQ9YKiIOTKn4/Sieu9Veu9pSkSUyzT6uWLqlvGB3a7pvXKQm','Jenette','Sparks','wyqulirem@mailinator.com','+1 (109) 983-1591',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-24 16:12:41',NULL,'2022-11-24 17:39:21',NULL),
(289,NULL,NULL,NULL,NULL,'$2y$10$Vmqt33UpxJvL1vCnLjCA8O2EVmZHLsgiXY./pROa/K0SxQT1/Eohy','Dieter','Byrd','nybowezen@mailinator.com','+1 (441) 676-8412',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,2,NULL,'2022-11-24 16:24:11',NULL,'2022-11-24 16:24:24',NULL),
(290,NULL,NULL,NULL,NULL,'$2y$10$bzrFhe0TFJMHVvrd9lohZOol7YwthCaq8S.tSPp91EMlP4jn17Vd2','Preston','Harding','pybamyjako@mailinator.com','+1 (827) 853-1911',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,2,NULL,'2022-11-24 16:29:38',NULL,'2022-11-24 16:29:53',NULL),
(291,NULL,NULL,NULL,NULL,'$2y$10$XOOfWoEqllohF4zTdCTXEe8bxxwo6Pl9g2gVCbPsN2DhlYBhDe1Hi','Admin','Admin','admin@gmail.com','+1 (795) 852-2044',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-24 17:10:32',NULL,'2022-11-24 17:10:32',NULL),
(292,NULL,NULL,NULL,NULL,'$2y$10$QS7yVfY1gIZjDJOR1fYDUOidmHQvz6b0pilrPhts5s9b473wOmJMW','Amanda','Moody','jimyj@mailinator.com','+1 (131) 191-3256',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'no',NULL,NULL,NULL,NULL,NULL,'yes',NULL,NULL,0,0,NULL,NULL,NULL,'not verified',NULL,2,1,NULL,'2022-11-24 17:39:09',NULL,'2022-11-24 17:39:09',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
