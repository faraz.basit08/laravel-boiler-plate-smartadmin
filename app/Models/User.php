<?php

namespace App\Models;

use App\Config\Constant;
use App\Utilities\Utilities;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class User extends Model
{
    use  Authenticatable,  HasFactory;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id',
        'company_id',
        'login_id',
        'emp_code',
        'emp_id',
        'password',
        'first_name',
        'last_name',
        'email_address',
        'phone_number',
        'gender',
        'profile_img',
        'date_of_birth',
        'user_type',
        'designation_id',
        'branch_id',
        'pb_code',
        'land_line',
        'ip',
        'date_of_joining',
        'date_of_resign',
        'date_of_transfer',
        'is_resigned',
        'creation_date',
        'android_push_id',
        'apple_push_id',
        'num_of_login_attempts',
        'last_login_time', 
        'is_new_user',
        'last_change_password_date',
        'token',
        'is_live' ,
        'temp_key',
        'device_id',
        'parallel_to',
        'email_verification_at',
        'email_verification_status',
        'activation_code',
        'status',
        'is_enable',
        'comments',
        'status',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'

    ];
    

    protected $tableColumnList = [
        'id',
        'company_id',
        'login_id',
        'emp_code',
        'emp_id',
        'password',
        'first_name',
        'last_name',
        'email_address',
        'phone_number',
        'gender',
        'profile_img',
        'date_of_birth',
        'user_type',
        'designation_id',
        'branch_id',
        'pb_code',
        'land_line',
        'ip',
        'date_of_joining',
        'date_of_resign',
        'date_of_transfer',
        'is_resigned',
        'creation_date',
        'android_push_id',
        'apple_push_id',
        'num_of_login_attempts',
        'last_login_time', 
        'is_new_user',
        'last_change_password_date',
        'token',
        'is_live' ,
        'temp_key',
        'device_id',
        'parallel_to',
        'email_verification_at',
        'email_verification_status',
        'activation_code',
        'status',
        'is_enable',
        'comments',
        'created_by',
        'created_at',
        'updated_at',
        'updated_by'
    ];

    protected $otherColumnList = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var string[]
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Scope a query to only include active records.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query

     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('is_enable', '!=', Constant::RecordType['DELETED']);
    }

    /**
     * Set column Name with Actual name.
     *
     * @param string $request
     * @return array
     */
    public function filterColumns(Request $request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $columnList = $this->tableColumnList + $this->otherColumnList;

         Utilities::filterColumnsModel($request, $columnList, $method);
    }
    /**
     * Get column for ordering after varification.
     *
     * @param string $field
     * @return string[]|array|string
     */
    public function getOrderColumn($field)
    {
        $columnList = $this->tableColumnList;

        foreach ($columnList as $key => $value) {
            if ($key === $field)
                return $value;
        }

        return "id";
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules($request, $method = null)
    {
        if ($method == null) {

            $method = $request->method();
        }

        $rules = [];

        $rules = match ($method) {
            'POST' => [
                'username' => [

                    'required', Rule::unique('users', 'username')->where(function ($query) use ($request) {
                        $query->where('is_enable', '<>', '2');
                    }),
                    'min:3', 'max:100' //'custom_alpha_num_dash_dot_nosp',
                ],
                'first_name' => 'required|min:3|max:100', //custom_alpha_dash_dot
                'last_name' => 'required|min:3|max:100', //custom_alpha_dash_dot
                'full_name' => 'required|min:3|max:100', //custom_alpha_dash_dot
                'email' => [
                ],
            ],
            'PUT' => [
                'id' => 'required|integer',
                'username' => [
                    'required', Rule::unique('users', 'username')->where(function ($query) use ($request) {
                        $query->where('is_enable', '<>', '2')
                            ->where('id', '<>', $request->id);
                    }),
                ],
                'first_name' => 'required|min:3|max:100', //custom_alpha_dash_dot
                'last_name' => 'required|min:3|max:100', //custom_alpha_dash_dot
                'full_name' => 'required|min:3|max:100', //custom_alpha_dash_dot
                'email' => [
                ],
            ],
            'PATCH' => [
                'id' => 'required|integer',
                'activate' => 'required|numeric|between:0,1'
            ],
            'DELETE' => [
                'id' => 'required|integer',
            ],
            'GET_ONE' => [
                'id' => 'required|integer'
                // 'fields' => ''
            ],
            'GET_ALL' => [
                // 'fields' => ''
            ]
        };
        return $rules;
    }

    /**
     * Get the validation custom messages.
     *
     * @return array
     */
    public function messages($request, $method = null)
    {
        if ($method == null) {
            $method = $request->method();
        }

        $messages = [];

        $commonMessages = [

            'username.required' => [
                "code" => 10028,
                "message" => "Please provide username."
            ],
            'username.unique' => [
                "code" => 10029,
                "message" => "Please provide unique username."
            ],
            'username.custom_alpha_num_dash_dot_nosp' => [
                "code" => 10030,
                "message" => "The username may only contain alphabates, numbers, dots, dashes and underscores."
            ],
            'username.min' => [
                "string" => [
                    "code" => 10031,
                    "message" => "The username must be at least :min characters."
                ]
            ],
            'username.max' => [
                "string" => [
                    "code" => 10032,
                    "message" => "The username may not be greater than :max characters."
                ]
            ],

            'full_name.required' => [
                "code" => 10033,
                "message" => "Please provide full name."
            ],
            'full_name.custom_alpha_dash_dot' => [
                "code" => 10034,
                "message" => "The full name may only contain alphabates, dots, dashes underscores and spaces."
            ],
            'full_name.min' => [
                "string" => [
                    "code" => 10035,
                    "message" => "The full name must be at least :min characters."
                ]
            ],
            'full_name.max' => [
                "string" => [
                    "code" => 10036,
                    "message" => "The full name may not be greater than :max characters."
                ]
            ],

            'firstName.required' => [
                "code" => 10037,
                "message" => "Please provide first name."
            ],
            'firstName.custom_alpha_dash_dot' => [
                "code" => 10038,
                "message" => "The first name may only contain alphabates, dots, dashes underscores and spaces."
            ],
            'firstName.min' => [
                "string" => [
                    "code" => 10039,
                    "message" => "The first name must be at least :min characters."
                ]
            ],
            'firstName.max' => [
                "string" => [
                    "code" => 10040,
                    "message" => "The first name may not be greater than :max characters."
                ]
            ],

            'last_name.required' => [
                "code" => 10041,
                "message" => "Please provide last name."
            ],
            'last_name.custom_alpha_dash_dot' => [
                "code" => 10042,
                "message" => "The last name may only contain alphabates, dots, dashes underscores and spaces."
            ],
            'last_name.min' => [
                "string" => [
                    "code" => 10043,
                    "message" => "The last name must be at least :min characters."
                ]
            ],
            'last_name.max' => [
                "string" => [
                    "code" => 10044,
                    "message" => "The last name may not be greater than :max characters."
                ]
            ],

            'email.required' => [
                "code" => 10045,
                "message" => "Please provide email."
            ],
            'email.unique' => [
                "code" => 10046,
                "message" => "Please provide unique email."
            ],
            'email.email' => [
                "code" => 10047,
                "message" => "Please provide valid email."
            ],
            'email.min' => [
                "string" => [
                    "code" => 10048,
                    "message" => "The email must be at least :min characters."
                ]
            ],
            'email.max' => [
                "string" => [
                    "code" => 10049,
                    "message" => "The email may not be greater than :max characters."
                ]
            ],

            'phone.required' => [
                "code" => 10050,
                "message" => "Please provide phone number."
            ],
            'phone.numeric' => [
                "code" => 10051,
                "message" => "The phone number may only contain numbers."
            ],
            'phone.digits_between' => [
                "code" => 10052,
                "message" => "The phone number must between 8 or 15 digits."
            ],

            'address.custom_alpha_num_dash_dot_coma' => [
                "code" => 10053,
                "message" => "The address may only contain alphabates, numerics, dashes, undescores, dots, brackets and spaces."
            ],
            'address.min' => [
                "string" => [
                    "code" => 10054,
                    "message" => "The address must be at least :min characters."
                ]
            ],
            'address.max' => [
                "string" => [
                    "code" => 10055,
                    "message" => "The address may not be greater than :max characters."
                ]
            ],
        ];

        $idMessages = [
            'id.required' => [
                "code" => 10056,
                "message" => "Please provide user id."
            ],
            'id.integer' => [
                "code" => 10057,
                "message" => "Id must be an integer."
            ]
        ];

        $statusMessage = [
            'activate.required' => [
                "code" => 10058,
                "message" => "Please provide activate flag."
            ],
            'activate.numeric' => [
                "code" => 10059,
                "message" => "Activate flag must be an integer."
            ],
            'activate.between' => [
                'numeric' => [
                    "code" => 10060,
                    "message" => "The activate flag must be between :min and :max."
                ]
            ]
        ];

        // $messages = match ($method) {
        //     'POST' => $commonMessages,
        //     'PUT' => $commonMessages + $idMessages,
        //     'PATCH' => $idMessages + $statusMessage,
        //     'DELETE' => $idMessages,
        //     'GET_ONE' => $idMessages,
        //     'GET_ALL' => $messages = []
        // };
        return $messages;
    }

}
