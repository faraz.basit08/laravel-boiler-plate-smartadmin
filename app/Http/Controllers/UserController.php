<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Utilities\Utilities;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Config\ProjectConstant;
use App\Config\Constant;
use App\Response\UserResponse;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private $select_columns = [ 'id','company_id','login_id','emp_code','emp_id','password','first_name','last_name','email_address',
    'phone_number','gender','profile_img','date_of_birth','user_type','designation_id','branch_id','pb_code','land_line','ip','date_of_joining',
    'date_of_resign','date_of_transfer','is_resigned','creation_date','android_push_id','apple_push_id','num_of_login_attempts','last_login_time', 'is_new_user','last_change_password_date','token',
    'is_live' ,'temp_key','device_id','parallel_to','email_verification_at','email_verification_status','activation_code','status','is_enable', 'comments',
    'created_by','created_at','updated_at',
    'updated_by'];

    /**
     * This fucntion is called after validation fails in function $this->validate.
     *
     * @param Request $request
     * @param array $errors
     * @return \Illuminate\Http\JsonResponse
     */
//            dd(DB::connection('mysql')->getQueryLog());
//            dd($request->input('password'));
    protected function buildFailedValidationResponse(Request $request, array $errors)
    {
        return "Unprocesssable Entity.";
    }

    // public function loginsubmit(LoginRequest $request)
    // {
    //     DB::connection()->enableQueryLog();
    //     Log::debug('in login controller');
    //     $credentials = $request->getCredentials();
        
    //     Log::debug(print_r($credentials, true));

    //     $loginId = $credentials['email'];
    //     $password = $credentials['password'];

    //     if(!Auth::validate(['email' => $loginId, 'password' => $password])):
    //         Log::debug('not authenticated');

    //         $queries = \DB::getQueryLog();
    //         //Log::debug($queries);
    //         // dd($queries);
    //         session()->flash('message', 'Authorization Failed (Wrong Email or Password)');
    //         return redirect()->to('/');
    //     endif;    


    //     //$AuthenticateUserPasswordArr = User::AuthenticateUserPassword($loginId,$password);
    //     //Log::debug('Authenticated Arr'.$AuthenticateUserPasswordArr);

    //     $arrUserCred['email'] = $credentials['email'];
    //     $arrUserCred['password'] = bcrypt($credentials['password']);
    //     $user = Auth::getProvider()->retrieveByCredentials($arrUserCred);
    //     $queries = \DB::getQueryLog();
    //     //Log::debug($queries);
    //     $data = Auth::login($user);
    //     Log::debug('after authentication');
    //     Log::debug($user);
    //     session(['userid' => $user->id]);
    //     session(['full_name' => $user->name]);
    //     session(['email' => $user->email]);
    //     session(['user_type' => $user->user_type]);
       
    //     if ($user->user_type == config('global.USER_TYPE_EMPLOYEE')) {
    //         // die();
    //         return redirect('/dashboard_employee');
    //        // return view('dashboard_employee');
    //     }elseif ($user->user_type == config('global.USER_TYPE_ADMIN'))
    //     {
    //         return view('dashboard_admin');
    //     }
    // }

    public function login()
    {
        return view('login');
    }
    
    public function loginSubmit(Request $request){
        $data = [];
        if ($request['email_address'] && $request['password']) {
            $user = User:: where("email_address", "=", $request->input('email_address'))
                ->first();
            if($user)
            {
                if(Hash::check($request['password'], $user->password))
                {
                    // $data['token'] =  $user->createToken('MyApp')->accessToken;
                    $data['name'] =  $user->email_address;
                    session(['id' => $user->id]);
                    session(['full_name' => $user->name]);
                    session(['email' => $user->email]);
                    session(['user_type' => $user->user_type]);
    //              $user->save();
                    return redirect()->route('user_index');
                }
                else {
                    return "Incorrect password, login failed!!.";
                }
            }
            else {
                return "Incorrect email address, login failed!!.";
            }
            
        } else {
            return "The login information is incomplete, please enter your username and password to log in!.";
        }
    }
    
    public function logout()
    {
        $userId = session()->get('id');
        if($userId != null)
        {
            Session::flush();
            return redirect()->route('login');
        }
        return redirect()->route('login'); // Added Temporary
    
    }
    
    public function index(Request $request)
    {
        $mdlUser = new User();
        // $userDept = User::with('user_roles_permission')->get()->toArray();
        $this->validate($request, $mdlUser->rules($request, Constant::RequestType['GET_ALL']), $mdlUser->messages($request, Constant::RequestType['GET_ALL']));
        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;
        
        
        $select = $this->select_columns;
        
        $mdlUser->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();

        if ($request->not_id != null) {
            $whereData[] = ['id', '!=', $request->not_id];
        }
        if ($request->username) {
            $whereData[] = ['username', 'LIKE', "%{$request->username}%"];
        }
        if ($request->firstName) {
            $whereData[] = ['first_name', 'LIKE', "%{$request->firstName}%"];
        }
        if ($request->lastName) {
            $whereData[] = ['last_name', 'LIKE', "%{$request->lastName}%"];
        }
        if ($request->full_name) {
            $whereData[] = ['full_name', 'LIKE', "%{$request->full_name}%"];
        }
        if ($request->phone) {
            $whereData[] = ['phone', 'LIKE', "%{$request->phone}%"];
        }
        if ($request->email) {
            $whereData[] = ['email', 'LIKE', "%{$request->email}%"];
        }
        if ($request->address) {
            $whereData[] = ['address', 'LIKE', "%{$request->address}%"];
        }
        if ($request->user_type != null) {
            $whereData[] = ['user_type', $request->user_type];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $orderBy =  $mdlUser->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $usersCount = User::where($whereData)->active()->get()->count();
        
        $users = User::where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);
        
        // foreach ($users as $user) {
        //     $user->user_roles_permission ?? null;
        // }
        
        // $status = 200;
        // $data_result = new UserResponse();
        // $data_result->setUsers($users->toArray());
        // $data_result->setTotalUsers($usersCount);
        // $response = Utilities::buildSuccessResponse(1013, "User list.", $data_result);
        $data['users'] = $users->toArray();

        return view('/user.index',$data);

        // return response()->json($response, $status);
    }


    public function create()
    {
        return view('/user.create');
    }

    public function store(Request $request)
    {
        $mdlUser = new User();
        // $this->validate($request, $mdlUser->rules($request), $mdlUser->messages($request));
        // $mdlUser->filterColumns($request);
        Utilities::defaultAddAttributes($request);
        $request = $request->all();
        $request['password'] = Hash::make($request['password']);
        $user = User::create($request);
        /**Take note of this: Your user authentication access token is generated here **/
//        $data['token'] =  $user->createToken('MyApp')->accessToken;
        $data['name'] =  $user->username;
        $data['id'] = $user->id;

        // $response = Utilities::buildSuccessResponse(1007, "User successfully created.", $data);
        // return response()->json($response, 201);
        return redirect()->route('user_index');

    }


        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $mdlUser = new User();
        
        $request->request->add(['id' => $id]);
        
        $this->validate($request, $mdlUser->rules($request, Constant::RequestType['GET_ONE']), $mdlUser->messages($request, Constant::RequestType['GET_ONE']));
        
        $select = $this->select_columns;
        $mdlUser->filterColumns($request);
        if ($request->fields) {
                $select = $request->fields;
            }
            
            $user = User::where('id', $request->id)->first($select);
        // $user->client ?? null;
        // $user->department ?? null;
        // $user->user_roles_permission ?? null;
        $status = 200;
        $response = [];
        if (!$user) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1011, "User not found.");
        } else {
            $dataResult = array("user" => $user->toArray());
            // $response = Utilities::buildSuccessResponse(1012, "User data.", $dataResult);
        }
        
        return view("/user.edit",$dataResult);

        // return response()->json($response, $status);
    }

    /**
     * Update User.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $mdlUser = new User();
        
        // $this->validate($request, $mdlUser->rules($request), $mdlUser->messages($request));

        // $mdlUser->filterColumns($request);

        Utilities::defaultUpdateAttributes($request);

        $user = User::find($request->id);


        $status = 200;
        $response = [];

        if (!$user) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1011, "User not found.");
        } else {

            $obj = $user->update($request->all());

            if ($obj) {
                $data = ['id' => $user->id];
                $response = Utilities::buildSuccessResponse(1008, "User successfully updated.", $data);
            }
        }
        return redirect()->route('user_index');
        // return response()->json($response, $status);
    }

    /**
     * Activate/De-Activate User.
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function patchDisableEnableUser(Request $request)
    {
        $mdlUser = new User();

        Utilities::removeAttributesExcept($request, ["id", "activate"]);

        $this->validate($request, $mdlUser->rules($request), $mdlUser->messages($request));

        Utilities::defaultUpdateAttributes($request);

        $activate = $request->activate == 1 ? Constant::RecordType['ENABLED'] : Constant::RecordType['DISABLED'];

        $request->request->add(['is_enable' => $activate]);

        $user = User::find($request->id);
        $status = 200;
        $response = [];

        if (!$user) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1011, "User not found.");
        } else {

            $obj = $user->update($request->all());

            if ($obj) {
                $data = ['id' => $user->id];
                $actMsg = $request->activate == 1 ? "activated" : "de-activated";
                $response = Utilities::buildSuccessResponse(1009, "User successfully $actMsg.", $data);
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Delete User.
     *
     * @param $id 'ID' of User to delete. (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {

        $mdlUser = new User();

        // $request->request->add(['id' => $request->id]);
        
        // $this->validate($request, $mdlUser->rules($request), $mdlUser->messages($request));

        Utilities::defaultDeleteAttributes($request);

        $request->request->add(['is_enable' => Constant::RecordType['DELETED']]);

        $user = User::find($request->id);

        $status = 200;
        $response = [];
        if (!$user) {
            $status = 404;
            $response = Utilities::buildBaseResponse(404, "User not found.");
        } else {

            $obj = $user->update($request->all());

            if ($obj) {
                $response = Utilities::buildBaseResponse(1, "User successfully deleted.");
            }
        }

        return response()->json($response, $status);
    }

    /**
     * Get one User.
     *
     * @param $id 'ID' of User to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOneUser($id, Request $request)
    {
        $mdlUser = new User();

        $request->request->add(['id' => $id]);

        $this->validate($request, $mdlUser->rules($request, Constant::RequestType['GET_ONE']), $mdlUser->messages($request, Constant::RequestType['GET_ONE']));

        $select = $this->select_columns;
        $mdlUser->filterColumns($request);
        if ($request->fields) {
            $select = $request->fields;
        }

        $user = User::where('id', $request->id)->first($select);
        // $user->client ?? null;
        // $user->department ?? null;
        // $user->user_roles_permission ?? null;

        $status = 200;
        $response = [];

        if (!$user) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1011, "User not found.");
        } else {
            $dataResult = array("user" => $user->toArray());
            $response = Utilities::buildSuccessResponse(1012, "User data.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Get one User roles and levels by keycloak id.
     *
     * @param $id 'ID' of User to return (required)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserRolesLevels($kc_id, Request $request)
    {
        $mdlUser = new User();
        $select = ['id', 'keycloak_id', 'user_type','client_id'];
        $mdlUser->filterColumns($request);
        if ($request->fields) {
            $select = $request->fields;
        }
        $user = User::where('keycloak_id', $request->kc_id)->first($select);
        $user->user_role_level_permission ?? null;

        $status = 200;
        $response = [];

        if (!$user) {
            $status = 404;
            $response = Utilities::buildBaseResponse(1011, "User not found.");
        } else {
            $dataResult = array("user" => $user->toArray());
            $response = Utilities::buildSuccessResponse(1014, "User role level.", $dataResult);
        }

        return response()->json($response, $status);
    }

    /**
     * Fetch list of User by searching with optional filters..
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUser(Request $request)
    {
        $mdlUser = new User();
        // $userDept = User::with('user_roles_permission')->get()->toArray();
        $this->validate($request, $mdlUser->rules($request, Constant::RequestType['GET_ALL']), $mdlUser->messages($request, Constant::RequestType['GET_ALL']));
        $pageSize = $request->limit ?? ProjectConstant::PageSize;
        if ($pageSize > ProjectConstant::MaxPageSize) {
            $pageSize = ProjectConstant::PageSize;
        }
        $page = $request->page ?? ProjectConstant::Page;
        $skip = ($page - 1) * $pageSize;


        $select = $this->select_columns;

        $mdlUser->filterColumns($request);

        if ($request->fields) {
            $select = $request->fields;
        }

        $whereData = array();

        if ($request->not_id != null) {
            $whereData[] = ['id', '!=', $request->not_id];
        }
        if ($request->username) {
            $whereData[] = ['username', 'LIKE', "%{$request->username}%"];
        }
        if ($request->firstName) {
            $whereData[] = ['first_name', 'LIKE', "%{$request->firstName}%"];
        }
        if ($request->lastName) {
            $whereData[] = ['last_name', 'LIKE', "%{$request->lastName}%"];
        }
        if ($request->full_name) {
            $whereData[] = ['full_name', 'LIKE', "%{$request->full_name}%"];
        }
        if ($request->phone) {
            $whereData[] = ['phone', 'LIKE', "%{$request->phone}%"];
        }
        if ($request->email) {
            $whereData[] = ['email', 'LIKE', "%{$request->email}%"];
        }
        if ($request->address) {
            $whereData[] = ['address', 'LIKE', "%{$request->address}%"];
        }
        if ($request->user_type != null) {
            $whereData[] = ['user_type', $request->user_type];
        }
        if ($request->activate != null) {
            $whereData[] = ['is_enable', $request->activate];
        }

        $orderBy =  $mdlUser->getOrderColumn($request->order_by);
        $orderType = $request->order_type ?? ProjectConstant::OrderType;

        $usersCount = User::where($whereData)->active()->get()->count();

        $users = User::where($whereData)
        ->active()
        ->orderBy($orderBy, $orderType)
        ->offset($skip)
        ->limit($pageSize)
        ->get($select);

        foreach ($users as $user) {
            $user->user_roles_permission ?? null;
        }

        $status = 200;
        $data_result = new UserResponse();
        $data_result->setUsers($users->toArray());
        $data_result->setTotalUsers($usersCount);
        $response = Utilities::buildSuccessResponse(1013, "User list.", $data_result);

        return response()->json($response, $status);
    }

}