@extends('layout/masterlayout')
@section('content')

<div class="row">
    <div class="col-xl-6">
        <div id="panel-1" class="panel">
            <div class="panel-hdr">
                <h2>
                    Create <span class="fw-300"><i>User</i></span>
                </h2>
                <div class="panel-toolbar">
                    <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Collapse"></button>
                    <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip"
                        data-offset="0,10" data-original-title="Fullscreen"></button>
                    <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Close"></button>
                </div>
            </div>
            <div class="panel-container show">
                <div class="panel-content">
                    <form method="POST" action="{{route('user_post')}}">
                        @csrf
                        <div class="form-group">
                            <label class="form-label" for="simpleinput">First Name</label>
                            <input type="text" id="first_name" name="first_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="simpleinput">Last Name</label>
                            <input type="text" id="last_name" name="last_name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="example-email-2">Email</label>
                            <input type="email" id="email_address" name="email_address" class="form-control"
                                placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="example-password">Password</label>
                            <input type="password" id="password" name="password" class="form-control" value="password">
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="example-palaceholder">Phone Number</label>
                            <input type="text" id="phone_number" name="phone_number" class="form-control"
                                placeholder="Phone Number">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary waves-effect waves-themed" type="submit"><i
                                    class="fas fa-plus" style="margin-right: 4px"></i>Add User</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection