@extends('layout/masterlayout')
@section('content')
<div class="subheader">
    <h1 class="subheader-title">
        <i class='subheader-icon fal fa-table'></i> Users
    </h1>
    <a href="{{route('user_create')}}" class="btn btn-primary float-right waves-effect waves-themed"><i class="fas fa-plus"
            style="margin-right: 4px"></i>Add User</a>
</div>
<div class="row">
    <div class="col-xl-12">
        <div id="panel-1" class="panel">
            <div class="panel-hdr">
                <h2>
                    Users <span class="fw-300"><i>View</i></span>
                </h2>
                <div class="panel-toolbar">
                    <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Collapse"></button>
                    <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip"
                        data-offset="0,10" data-original-title="Fullscreen"></button>
                    <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10"
                        data-original-title="Close"></button>
                </div>
            </div>
            <div class="panel-container show">
                <div class="panel-content">
                    <!-- datatable start -->
                    <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                        <thead>
                            <tr>
                                <th>Serial</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Status</th>
                                <th>Admin Controls</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td> {{$loop->iteration ?? ''}}</td>
                                <td>{{$user['first_name'] ?? ''}}</td>
                                <td>{{$user['last_name'] ?? ''}}.</td>
                                <td>{{$user['email_address'] ?? ''}}</td>
                                <td>{{$user['phone_number'] ?? ''}}</td>
                                <td>{{$user['status'] ?? ''}}</td>
                                <td>
                                    <center>
                                        <a href="{{route('user_edit',$user['id'])}}" onclick="" title="Edit User"
                                            class="btn btn-sm btn-primary waves-effect waves-themed"
                                            data-original-title="View" style="margin-right: 5px;"><i
                                                class="fal fa-edit"></i></a>
                                        <a href="#" onclick="deleteUser({{$user['id']}})" title="Inactive User"
                                            class="btn btn-sm btn-primary waves-effect waves-themed"
                                            data-original-title="Delete" style="margin-right: 5px;"><i
                                                class="fal fa-trash"></i></a>
                                        <a href="#" onclick="" title="User Detail"
                                            class="btn btn-sm btn-primary waves-effect waves-themed"
                                            data-original-title="View" style="margin-right: 5px;"><i
                                                class="fal fa-eye"></i></a>
                                        <a href="#" onclick="" title="Resigned User"
                                            class="btn btn-sm btn-primary waves-effect waves-themed"
                                            data-original-title="Resigned" style="margin-right: 5px;"><i
                                                class="fal fa-user icon"></i></a>
                                        <!-- <a href="#" onclick="" title="Transfered User"
                                            class="btn btn-sm btn-primary waves-effect waves-themed"
                                            data-original-title="Transfered" style="margin-right: 5px;"><i
                                                class="fal fa-exchange icon"></i></a> -->
                                        <!-- <a href="#" onclick="" title="User Mapping"
                                            class="btn btn-sm btn-primary waves-effect waves-themed"
                                            data-original-title="User Mapping" style="margin-right: 5px;"><i
                                                class="fal fa-users icon"></i></a>
                                        <a href="#" onclick="" title="Reset User"
                                            class="btn btn-sm btn-primary waves-effect waves-themed"
                                            data-original-title="Reset User" style="margin-right: 5px;"><i
                                                class="fal fa-refresh"></i></a> -->
                                    </center>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Serial</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Status</th>
                                <th>Admin Controls</th>
                            </tr>
                        </tfoot>
                    </table>
                    <!-- datatable end -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('datatable_js')
<script src="{{asset('smartadmin/general/js/datagrid/datatables/datatables.bundle.js')}}"></script>
<script>
/* demo scripts for change table color */
/* change background */


$(document).ready(function() {
    $('#dt-basic-example').dataTable({
        responsive: true
    });

    $('.js-thead-colors a').on('click', function() {
        var theadColor = $(this).attr("data-bg");
        console.log(theadColor);
        $('#dt-basic-example thead').removeClassPrefix('bg-').addClass(theadColor);
    });

    $('.js-tbody-colors a').on('click', function() {
        var theadColor = $(this).attr("data-bg");
        console.log(theadColor);
        $('#dt-basic-example').removeClassPrefix('bg-').addClass(theadColor);
    });

});
</script>
<script>
function deleteUser(id) {
    userId = {
        "id": id
    };
    event.preventDefault(); // prevent form submit
    var form = event.target.form; // storing the form
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!"
    }).then(function(result) {
        if (result.value) {
            var request = $.ajax({
                url: "{{route('user_destroy')}}",
                type: "POST",
                data: {
                    "_token": "{{ csrf_token() }}",
                    id: id
                },
                json: "html"
            });
            request.done(function(msg) {
                if (msg.code == 1) {
                    Swal.fire("Deleted", "User Deleted", "success");
                    location.reload();
                }
                if (msg.code == 0) {
                    Swal.fire("Not Deleted", "User could not be deleted", "error");
                }
            });
            request.fail(function(jqXHR, textStatus) {
                alert("Request failed: " + textStatus);
            });

        } else {
            debugger
            Swal.fire("Cancelled", "User did not deleted", "error");
        }
        // Swal.fire("Deleted!", "Your file has been deleted.", "success");
    });

}
</script>
@endsection