<?php
namespace App\Config;

class Constant
{

    const ARR_VERSION = '1.1';
    
    const RequestType = [
        'GET_ONE' => "GET_ONE",
        'GET_ALL' => "GET_ALL"
    ];

    const RecordType = [
        'DISABLED' => 0,
        'PENDING' => 2,
        'DELETED' => 2
    ];
    
    const LangDir = [
        'RTL' => 1,
        'LTR' => 2,
    ];
    
    const Frequencies = [
        'YEAR', 'MONTH', 'WEEK', 'DAY'
    ];
    
    const PermissionsMethod = [
        'POST', 'PUT', 'PATCH', 'DELETE', 'GET'
    ];
    
    const PermissionsLabel = [
        'Add', 'Edit', 'Change Status', 'Delete', 'View'
    ];
    
    const Page = 1;
    const PageSize = 100;
    const MaxPageSize = 500;
    const OrderType = 'desc';
}
